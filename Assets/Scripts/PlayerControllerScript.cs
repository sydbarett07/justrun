﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerScript : MonoBehaviour {

    [SerializeField] private float m_MaxSpeed = 10f;                    
    [SerializeField] private float m_JumpForce = 450f;                  
    [SerializeField] private LayerMask m_WhatIsGround;                  
    [SerializeField] private GameObject player;
    [SerializeField] private LayerMask m_NotfallThrough;

    //public float move = 1f;
    private Transform m_GroundCheck;    
    const float k_GroundedRadius = .2f; 
    private bool m_Grounded;

    private bool notFallThrough;

  
    private Animator m_Anim;            
    private Rigidbody2D m_Rigidbody2D;
    private bool m_FacingRight = true;  

    public bool sliding = false;
    float slideTimer = 0f;
    public float maxSlideTime = 1.5f;

    // Shooting
    public Transform gunMuzzle;
    public GameObject bulletPrefab;
    public float shotsPersecond;
    private float nextShotTime;

/*#if UNITY_ANDRIOD || UNITY_IOS
    public float move = 1f;
#endif*/

    private void Awake()
    {
      
        m_Anim = GetComponent<Animator>();
        
    }
     void Start()
    {
        
        m_GroundCheck = transform.Find("GroundCheck");
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

    }
    void Update()
    {
        if (Input.GetButtonDown("Shoot"))
            Shoot();
        if (Input.GetButtonDown("Jump"))
            Jump();
        if (Input.GetButtonDown("Fall"))
            FallThrough();
        if (Input.GetButtonDown("Slide"))
            Sliding();
        else if (Input.GetButtonUp("Slide"))
            FixedUpdate();

    }

    private void FixedUpdate()
    {
        m_Grounded = false;
        notFallThrough = false;
        
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
                m_Grounded = true;
        }
        m_Anim.SetBool("Ground", m_Grounded);

        // Set the vertical animation
        m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);
        Move();

        if (sliding)
        {
            slideTimer += Time.deltaTime;

            if (slideTimer > maxSlideTime)
            {
                sliding = false;

                m_Anim.SetBool("isSliding", false);
                player.GetComponent<CircleCollider2D>().enabled = true;
            }
        }
        
    }

    public void Jump()
    {
        // If the player should jump...
        if (m_Grounded && m_Anim.GetBool("Ground"))
        {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Anim.SetBool("Ground", false);
            m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
        }
    }

    public void FallThrough()
    {
        Collider2D[] colliders1 = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_NotfallThrough);
        for (int i = 0; i < colliders1.Length; i++)
        {
            if (colliders1[i].gameObject != gameObject)
                notFallThrough = true;
        }
        Debug.Log(notFallThrough);
        if (m_Grounded && notFallThrough == false)
        {
            player.GetComponent<CircleCollider2D>().enabled = false;
            player.GetComponent<BoxCollider2D>().enabled = false;
            Invoke("PlayerColliderInAction", 0.5999f);
        }
       
    }

    
    public void PlayerColliderInAction()
    {
        player.GetComponent<CircleCollider2D>().enabled = true;
        player.GetComponent<BoxCollider2D>().enabled = true;
    }
    public void Shoot()
    {
        if (Time.time > nextShotTime)
        {
            nextShotTime = Time.time + 1f / shotsPersecond;
           // bulletPrefab.Spawn(gunMuzzle.position, gunMuzzle.rotation);
        }
    }

    public void Sliding()
    {
        if (!sliding)
        {
            slideTimer = 0f;

            m_Anim.SetBool("isSliding", true);
            player.GetComponent<CircleCollider2D>().enabled = false;
            

            sliding = true;

        }
    }

    public void Move()
    {
        
        if (m_Grounded)
        {
           
            m_Anim.SetFloat("Speed", Mathf.Abs(1));

            
            m_Rigidbody2D.velocity = new Vector2(1 * m_MaxSpeed, m_Rigidbody2D.velocity.y);

           
            if (!m_FacingRight)
            {
                
                Flip();
            }
        }
    }
    

    void Flip()
    {
        
        m_FacingRight = !m_FacingRight;

        
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

   /*void OnCollisionEnter2D(Collision2D collision)
    {
        // Debug.Log(collision.gameObject);
        
            if (collision.gameObject.tag == "Midground")
            {
                Debug.Log(collision.gameObject);
                collision.gameObject.GetComponent<BoxCollider2D>().enabled = false;

            }

        
        
    }*/

}


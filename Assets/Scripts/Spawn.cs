﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

    public Transform generationPoint;

    private float distanceBetween;
    public float distanceBetweenMin;
    public float distanceBetweenMax;

    private float width;
    ObjectPooler objectPool;
    public float height;

    void Start()
    {
        objectPool = ObjectPooler.SharedInstance;
        
    }

    void Update()
    {
        if (transform.position.x < generationPoint.position.x)
        {

            distanceBetween = Random.Range(distanceBetweenMin, distanceBetweenMax);
            int index = Random.Range(0, 3);
            width = objectPool.GetPooledObject(index).GetComponent<BoxCollider2D>().size.x;
            transform.position = new Vector3(transform.position.x + (width / 2) + distanceBetween, transform.position.y, transform.position.z);


            GameObject newPlatform = objectPool.GetPooledObject(index);
            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;
            newPlatform.SetActive(true);

            transform.position = new Vector3(transform.position.x + (width / 2), transform.position.y, transform.position.z);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyerScript : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Debug.Break();
            return;
        }

        if(collision.gameObject.transform.parent)
        {
            gameObject.SetActive(false);
        }
        else
        {
            collision.gameObject.SetActive(false);
        }

        
    }
}
